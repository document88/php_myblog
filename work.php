<?php
// 載入db.php 讀取數據庫並存入session
require_once 'php/db.php';
// 載入functions.php SQL語句
require_once 'php/functions.php';
// 獲取文章列表
if (isset($_GET['id'])) {
$get_work = get_work($_GET['id']);
}
?>

<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-<?php echo $get_work['title']; ?></title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 標題選單 -->
  <?php
  require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main">

    <!-- 如果資料庫的文章不為空則渲染 -->
    <?php if (!empty($get_work)) : ?>
      <div class="works get_work_id">
      <h3 class="title"><?php echo $get_work['title']; ?></h3>
        <div class="work_left">
          <?php if ($get_work['image_path']) : ?>
            <img src='<?php echo $get_work['image_path']; ?>' class="img-responsive" id="apngImage" onclick="restartAPNG()">
          <?php else : ?>
            <video src="<?php echo $get_work['video_path']; ?>" controls></video>
          <?php endif; ?>
        </div>
        <div class="contents">
          <?php
          //去除所有html標籤
          $abstract = strip_tags($get_work['intro']);
          ?>
          <h3 class="abstract"><?php echo $abstract; ?></h3>
          <span class="time"><?php echo $get_work['upload_date']; ?> / 作者: <?php echo $get_work['name']; ?></span>
        </div>
      </div>
    <?php else : ?>
      <h4 class="no_works">尚無文章</h4>
    <?php endif; ?>
  </div>
  <!-- 底部 -->
  <?php
  require_once 'components/footer.php';
  ?>
  <script>
    function restartAPNG() {
      var apngImage = document.getElementById('apngImage');
      apngImage.src = ''; // 清空src
      apngImage.src = '<?php echo $get_work['image_path']; ?>'; // 重新設置src，觸發重新加載
    }
  </script>
</body>

</html>