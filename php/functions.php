<?php
// 啟用session
@session_start();
// 獲取文章的方法
function get_publish_article()
{
  // 用來放文章的陣列
  $datas = array();
  // SQL語句 獲取: 所有, 從: 資料庫article, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `article` WHERE `publish` = 1;";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) > 0) {
      //每次獲取一筆資料直到$row = null
      while ($row = mysqli_fetch_assoc($query)) {
        // 對datas的末端插入當前循環所獲取的一筆資料
        $datas[] = $row;
      }
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $datas;
}
// 獲取文章的方法id
function get_article($id)
{
  // 用來放文章的陣列
  $result = null;
  // SQL語句 獲取: 所有, 從: 資料庫article, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `article` WHERE `publish` = 1 AND `id` = '{$id}';";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) == 1) {
      $result = mysqli_fetch_assoc($query);
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $result;
}
// 獲取文章的方法search
function get_search_article($search)
{
  // 用來放文章的陣列
  $datas = array();
  // SQL語句 獲取: 所有, 從: 資料庫article, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `article` 
  WHERE `publish` = 1 
    AND (`title` LIKE '%$search%' OR `content` LIKE '%$search%' OR `category` = '$search' OR `create_date` LIKE '%$search%' OR `name` LIKE '%$search%');";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) > 0) {
      //每次獲取一筆資料直到$row = null
      while ($row = mysqli_fetch_assoc($query)) {
        // 對datas的末端插入當前循環所獲取的一筆資料
        $datas[] = $row;
      }
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $datas;
}
// 獲取作品的方法
function get_publish_works()
{
  // 用來放作品的陣列
  $datas = array();
  // SQL語句 獲取: 所有, 從: 資料庫work, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `works` WHERE `publish` = 1;";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) > 0) {
      //每次獲取一筆資料直到$row = null
      while ($row = mysqli_fetch_assoc($query)) {
        // 對datas的末端插入當前循環所獲取的一筆資料
        $datas[] = $row;
      }
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $datas;
}
// 獲取作品的方法id
function get_work($id)
{
  // 用來放作品的陣列
  $result = null;
  // SQL語句 獲取: 所有, 從: 資料庫works, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `works` WHERE `publish` = 1 AND `id` = {$id};";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) == 1) {
      $result = mysqli_fetch_assoc($query);
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $result;
}
// 獲取作品的方法search
function get_search_work($search)
{
  // 用來放作品的陣列
  $datas = array();
  // SQL語句 獲取: 所有, 從: 資料庫article, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `works` WHERE `publish` = 1 AND (`title` LIKE '%$search%' OR `intro` LIKE '%$search%' OR `upload_date` LIKE '%$search%' OR `name` LIKE '%$search%');";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) > 0) {
      //每次獲取一筆資料直到$row = null
      while ($row = mysqli_fetch_assoc($query)) {
        // 對datas的末端插入當前循環所獲取的一筆資料
        $datas[] = $row;
      }
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $datas;
}
// 確認帳號是否存在username
function check_has_username($username)
{
  // 用來放帳號
  $result = null;
  // SQL語句 獲取: 所有, 從: 資料庫user, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `user` WHERE `username` = '{$username}';";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) >= 1) {
      $result = true;
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $result;
}
// 註冊新增使用者username,password,name
function add_user($username, $password, $name)
{
  // 用來放帳號
  $result = null;
  // 密碼加密
  $password = md5($password);
  // SQL語句 獲取: 所有, 從: 資料庫user, 條件: 發布狀況publish=1
  $sql = "INSERT into `user` (`username`, `password`, `name`) value('{$username}','{$password}', '{$name}')
  ;";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_affected_rows($_SESSION['link']) == 1) {
      $result = true;
    }
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $result;
}
// 登入使用者username,password(紀錄user_id,user_name)
function verify_user($username, $password)
{
  // 用來放帳號
  $result = null;
  // 密碼加密
  $password = md5($password);
  // SQL語句 獲取: 所有, 從: 資料庫user, 條件: 帳號密碼
  $sql = "SELECT * FROM `user` WHERE `username` = '{$username}' AND `password` = '{$password}';";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) >= 1) {
      //取得使用者資料
      $user = mysqli_fetch_assoc($query);
      //紀錄登入者的id
      $_SESSION['login_user_id'] = $user['id'];
      $_SESSION['login_user_username'] = $user['username'];
      $_SESSION['login_user_name'] = $user['name'];
      // 紀錄登入狀態
      $_SESSION['is_login'] = true;
      $result = true;
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $result;
}
// 後台獲取文章的方法user_id
function get_all_article($user_id)
{
  // 用來放文章的陣列
  $datas = array();
  // SQL語句 獲取: 所有, 從: 資料庫article, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `article` WHERE `creater_id` = '{$user_id}';";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) > 0) {
      //每次獲取一筆資料直到$row = null
      while ($row = mysqli_fetch_assoc($query)) {
        // 對datas的末端插入當前循環所獲取的一筆資料
        $datas[] = $row;
      }
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $datas;
}
// 後台獲取文章的方法user_id,search
function admin_get_search_article($user_id, $search)
{
  // 用來放文章的陣列
  $datas = array();
  // SQL語句 獲取: 所有, 從: 資料庫article, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `article` WHERE `creater_id` = '{$user_id}' AND (`title` LIKE '%$search%' OR `content` LIKE '%$search%' OR `category` = '$search' OR `create_date` LIKE '%$search%' OR `name` LIKE '%$search%');";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) > 0) {
      //每次獲取一筆資料直到$row = null
      while ($row = mysqli_fetch_assoc($query)) {
        // 對datas的末端插入當前循環所獲取的一筆資料
        $datas[] = $row;
      }
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $datas;
}
// 後台新增文章
function add_article($title, $category, $content, $publish)
{
  //宣告要回傳的結果
  $result = null;
  //建立現在的時間
  $create_date = date("Y-m-d H:i:s");
  //內容處理html
  $title = htmlspecialchars($title);
  $content1 = strip_tags($content, '<a>');
  $content1 = htmlspecialchars($content1);
  //內容處理html簡略版
  $content_little = strip_tags($content);
  $content_little = htmlspecialchars($content_little);
  //取得100個字
  if (mb_strlen($content_little, 'UTF-8') > 100) {
    $content_little = mb_substr($content_little, 0, 100, 'UTF-8') . '...';
  }
  //取得登入者的id, name
  $creater_id = $_SESSION['login_user_id'];
  $creater_name = $_SESSION['login_user_name'];
  //新增語法
  $sql = "INSERT INTO `article` (`title`, `category`, `content`, `content_little`, `publish`, `create_date`, `creater_id`, `name`)
  				VALUE ('{$title}', '{$category}', '{$content1}', '{$content_little}', {$publish}, '{$create_date}', '{$creater_id}', '{$creater_name}');";

  //用 mysqli_query 方法取執行請求（也就是sql語法），請求後的結果存在 $query 變數中
  $query = mysqli_query($_SESSION['link'], $sql);

  //如果請求成功
  if ($query) {
    //使用 mysqli_affected_rows 判別異動的資料有幾筆，基本上只有新增一筆，所以判別是否 == 1
    if (mysqli_affected_rows($_SESSION['link']) == 1) {
      //取得的量大於0代表有資料
      //回傳的 $result 就給 true 代表有該帳號，不可以被新增
      $result = true;
    }
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息：" . mysqli_error($_SESSION['link']);
  }

  //回傳結果
  return $result;
}
// 後台獲取要編輯文章的方法id
function admin_get_article($id)
{
  // 用來放文章的陣列
  $result = null;
  // SQL語句 獲取: 所有, 從: 資料庫article, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `article` WHERE `id` = '{$id}';";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) == 1) {
      $result = mysqli_fetch_assoc($query);
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $result;
}
// 後台修改文章
function admin_updata_article($id, $title, $category, $content, $publish)
{
  //宣告要回傳的結果
  $result = null;
  //取得登入者的 name
  $creater_name = $_SESSION['login_user_name'];
  //建立現在的時間
  $modify_date = date("Y-m-d H:i:s");
  //內容處理html
  $title = htmlspecialchars($title);
  $content1 = strip_tags($content, '<a>');
  $content1 = htmlspecialchars($content1);
  //內容處理html簡略版
  $content_little = strip_tags($content);
  $content_little = htmlspecialchars($content_little);
  //取得100個字
  if (mb_strlen($content_little, 'UTF-8') > 100) {
    $content_little = mb_substr($content_little, 0, 100, 'UTF-8') . '...';
  }
  //更新語法
  $sql = "UPDATE `article` SET `title` = '{$title}', `category` = '{$category}', `content` = '{$content1}', `content_little` = '{$content_little}', `publish` = {$publish}, `modify_date` = '{$modify_date}', `name` = '{$creater_name}'
  				WHERE `id` = {$id};";

  //用 mysqli_query 方法取執行請求（也就是sql語法），請求後的結果存在 $query 變數中
  $query = mysqli_query($_SESSION['link'], $sql);

  //如果請求成功
  if ($query) {
    //使用 mysqli_affected_rows 判別異動的資料有幾筆，基本上只有新增一筆，所以判別是否 == 1
    if (mysqli_affected_rows($_SESSION['link']) == 1) {
      //取得的量大於0代表有資料
      //回傳的 $result 就給 true 代表有該帳號，不可以被新增
      $result = true;
    }
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息：" . mysqli_error($_SESSION['link']);
  }

  //回傳結果
  return $result;
}
// 後台刪除文章
function admin_del_article($id)
{
  //宣告要回傳的結果
  $result = null;
  //刪除語法
  $sql = "DELETE FROM `article` WHERE `id` = {$id};";

  //用 mysqli_query 方法取執行請求（也就是sql語法），請求後的結果存在 $query 變數中
  $query = mysqli_query($_SESSION['link'], $sql);

  //如果請求成功
  if ($query) {
    //使用 mysqli_affected_rows 判別異動的資料有幾筆，基本上只有新增一筆，所以判別是否 == 1
    if (mysqli_affected_rows($_SESSION['link']) == 1) {
      //取得的量大於0代表有資料
      //回傳的 $result 就給 true 代表有該帳號，不可以被新增
      $result = true;
    }
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息：" . mysqli_error($_SESSION['link']);
  }

  //回傳結果
  return $result;
}
// 獲取作品的方法-用戶id-search
function get_work_id_search($id, $search)
{
  // 用來放作品的陣列
  $datas = array();
  // SQL語句 獲取: 所有, 從: 資料庫article, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `works` WHERE `create_user_id` = {$id} AND (`title` LIKE '%$search%' OR `intro` LIKE '%$search%' OR `upload_date` LIKE '%$search%' OR `name` LIKE '%$search%');";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) > 0) {
      //每次獲取一筆資料直到$row = null
      while ($row = mysqli_fetch_assoc($query)) {
        // 對datas的末端插入當前循環所獲取的一筆資料
        $datas[] = $row;
      }
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $datas;
}
// 後台獲取作品-會員id
function get_userid_all_works($id)
{
  // 用來放獲取的數據
  $datas = array();
  // SQL語句 獲取: 所有, 從: 資料庫work, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `works` WHERE `create_user_id` = '{$id}';";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) > 0) {
      //每次獲取一筆資料直到$row = null
      while ($row = mysqli_fetch_assoc($query)) {
        // 對datas的末端插入當前循環所獲取的一筆資料
        $datas[] = $row;
      }
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $datas;
}
// 後台新增作品-標題-圖檔位置-影片位置-發布狀態
function add_work($title, $intro, $image_path, $video_path, $publish)
{
  //宣告要回傳的結果
  $result = null;

  //內容處理html
  $title = htmlspecialchars($title);
  $intro = htmlspecialchars($intro);
  //建立者id, name
  $create_user_id = $_SESSION['login_user_id'];
  $creater_name = $_SESSION['login_user_name'];
  //上傳時間
  $upload_date = date("Y-m-d H:i:s");

  //處理圖片路徑
  $image_path_value = "'{$image_path}'";
  if ($image_path == '') $image_path_value = 'NULL';

  //處理影片路徑
  $video_path_value = "'{$video_path}'";
  if ($video_path == '') $video_path_value = 'NULL';

  //新增語法
  $sql = "INSERT INTO `works` (`title`, `intro`, `image_path`, `video_path`, `publish`, `upload_date`, `create_user_id`, `name`)
  				VALUE ('{$title}', '{$intro}', {$image_path_value}, {$video_path_value}, {$publish}, '{$upload_date}', '{$create_user_id}', '{$creater_name}');";


  //用 mysqli_query 方法取執行請求（也就是sql語法），請求後的結果存在 $query 變數中
  $query = mysqli_query($_SESSION['link'], $sql);

  //如果請求成功
  if ($query) {
    //使用 mysqli_affected_rows 判別異動的資料有幾筆，基本上只有新增一筆，所以判別是否 == 1
    if (mysqli_affected_rows($_SESSION['link']) == 1) {
      //取得的量大於0代表有資料
      //回傳的 $result 就給 true 代表有該帳號，不可以被新增
      $result = true;
    }
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息：" . mysqli_error($_SESSION['link']);
  }

  //回傳結果
  return $result;
}
// 後台更新作品-id-標題-圖檔位置-影片位置-發布狀態
function update_work($id, $title, $intro, $image_path, $video_path, $publish)
{
  //宣告要回傳的結果
  $result = null;

  //取得登入者的 name
  $creater_name = $_SESSION['login_user_name'];
  //內容處理html
  $title = htmlspecialchars($title);
  $intro = htmlspecialchars($intro);
  $image_path_query = "`image_path` = '{$image_path}'";
  if ($image_path == '') {
    $image_path_query = "`image_path` = NULL";
  }

  $video_path_query = "`video_path` = '{$video_path}'";
  if ($video_path == '') {
    $video_path_query = "`video_path` = NULL";
  }
  //更新語法
  $sql = "UPDATE `works` SET `title` = '{$title}', `intro` = '{$intro}', {$image_path_query}, {$video_path_query}, `publish` = {$publish}, `name` = '{$creater_name}'
  				  WHERE `id` = {$id};";

  //用 mysqli_query 方法取執行請求（也就是sql語法），請求後的結果存在 $query 變數中
  $query = mysqli_query($_SESSION['link'], $sql);

  //如果請求成功
  if ($query) {
    //使用 mysqli_affected_rows 判別異動的資料有幾筆，基本上只有新增一筆，所以判別是否 == 1
    if (mysqli_affected_rows($_SESSION['link']) == 1) {
      //取得的量大於0代表有資料
      //回傳的 $result 就給 true 代表有該帳號，不可以被新增
      $result = true;
    }
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息：" . mysqli_error($_SESSION['link']);
  }

  //回傳結果
  return $result;
}
// 後台獲取作品的方法-作品id
function get_work_workid($id)
{
  // 用來放作品的陣列
  $result = null;
  // SQL語句 獲取: 所有, 從: 資料庫works, 條件: 發布狀況publish=1
  $sql = "SELECT * FROM `works` WHERE `id` = {$id};";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) == 1) {
      $result = mysqli_fetch_assoc($query);
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $result;
}
// 後台刪除作品-id
function del_work($id)
{
  //宣告要回傳的結果
  $result = null;

  //先取得該作品資訊
  $work = get_work_workid($id);

  if ($work) {
    //有作品才進行刪除工作
    //若有圖檔資料，以及圖檔有存在，就刪除
    if ($work['image_path'] && file_exists("../" . $work['image_path'])) {
      //unlink 為刪除檔案的方法，把上一層找到 files/ 裡面的檔案，做刪除
      unlink("../" . $work['image_path']);
    }

    //若有影片檔資料，以及影片檔有存在，就刪除
    if ($work['video_path'] && file_exists("../" . $work['video_path'])) {
      //unlink 為刪除檔案的方法，把上一層找到 files/ 裡面的檔案，做刪除
      unlink("../" . $work['video_path']);
    }

    //刪除作品語法
    $sql = "DELETE FROM `works` WHERE `id` = {$id};";

    //用 mysqli_query 方法取執行請求（也就是sql語法），請求後的結果存在 $query 變數中
    $query = mysqli_query($_SESSION['link'], $sql);

    //如果請求成功
    if ($query) {
      //使用 mysqli_affected_rows 判別異動的資料有幾筆，基本上只有新增一筆，所以判別是否 == 1
      if (mysqli_affected_rows($_SESSION['link']) == 1) {
        //取得的量大於0代表有資料
        //回傳的 $result 就給 true 代表有該帳號，不可以被新增
        $result = true;
      }
    } else {
      echo "{$sql} 語法執行失敗，錯誤訊息：" . mysqli_error($_SESSION['link']);
    }
  }

  //回傳結果
  return $result;
}
// 後台獲取會員資訊id(紀錄user_id,user_name)
function admin_member_user($id)
{
  // 用來放數據
  $result = null;
  // 密碼加密
  // SQL語句 獲取: 所有, 從: 資料庫user, 條件: 會員ID
  $sql = "SELECT * FROM `user` WHERE `id` = '{$id}';";
  // 執行從存在session的link資料庫執行上方的SQL獲取語句
  $query = mysqli_query($_SESSION['link'], $sql);
  //如果請求成功
  if ($query) {
    //判別獲取的資料數量是否大於0
    if (mysqli_num_rows($query) >= 1) {
      //取得使用者資料
      $user = mysqli_fetch_assoc($query);
      //紀錄登入者的id
      $_SESSION['login_user_id'] = $user['id'];
      $_SESSION['login_user_username'] = $user['username'];
      $_SESSION['login_user_name'] = $user['name'];
      // 紀錄登入狀態
      $_SESSION['is_login'] = true;
      $result = true;
    }
    //釋放資料庫查詢到的記憶體
    mysqli_free_result($query);
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息： " . mysqli_error($_SESSION['link']);
  }
  //回傳結果
  return $result;
}
// 後台編輯個人資料
function update_member($id, $name, $password)
{
  //宣告要回傳的結果
  $result = null;
  //根據有無 password 給予不同的 語法
  if ($password) {
    //有直代表要改密碼
    $password = md5($password);
    //更新語法
    $sql = "UPDATE `user` SET `password` = '{$password}', `name` = '{$name}'
	  				WHERE `id` = {$id};";
  } else {
    //沒有就不用
    //更新語法
    $sql = "UPDATE `user` SET `name` = '{$name}'
	  				WHERE `id` = {$id};";
  }

  //用 mysqli_query 方法取執行請求（也就是sql語法），請求後的結果存在 $query 變數中
  $query = mysqli_query($_SESSION['link'], $sql);

  //如果請求成功
  if ($query) {
    //使用 mysqli_affected_rows 判別異動的資料有幾筆，基本上只有新增一筆，所以判別是否 == 1
    if (mysqli_affected_rows($_SESSION['link']) == 1) {
      //取得的量大於0代表有資料
      $result = true;
    }
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息：" . mysqli_error($_SESSION['link']);
  }

  //回傳結果
  return $result;
}
// 後台刪除會員
function del_member($id)
{
  //宣告要回傳的結果
  $result = null;
  //刪除語法
  $sql = "DELETE FROM `user` WHERE `id` = {$id};";

  //用 mysqli_query 方法取執行請求（也就是sql語法），請求後的結果存在 $query 變數中
  $query = mysqli_query($_SESSION['link'], $sql);

  //如果請求成功
  if ($query) {
    //使用 mysqli_affected_rows 判別異動的資料有幾筆，基本上只有新增一筆，所以判別是否 == 1
    if (mysqli_affected_rows($_SESSION['link']) == 1) {
      //取得的量大於0代表有資料
      //回傳的 $result 就給 true 代表有該帳號，不可以被新增
      $result = true;
    }
  } else {
    echo "{$sql} 語法執行失敗，錯誤訊息：" . mysqli_error($_SESSION['link']);
  }

  //回傳結果
  return $result;
}
