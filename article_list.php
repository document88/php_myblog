<?php
// 載入db.php 讀取數據庫並存入session
require_once 'php/db.php';
// 載入functions.php SQL語句
require_once 'php/functions.php';
// 獲取文章列表
if (isset($_GET['search']) && !empty($_GET['search'])){
  $userInput = $_GET['search'];
  $search = mysqli_real_escape_string($_SESSION['link'], $userInput);
  $get_articles = get_search_article($search);
}else{
  $get_articles = get_publish_article();
}
?>

<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-文章</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 標題選單 -->
  <?php 
    require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main">
    <!-- 如果資料庫的文章不為空則渲染 -->
    <?php if (!empty($get_articles)) : ?>
      <?php foreach ($get_articles as $row) : ?>
        <!-- 用$_GET來獲取, ?參=值 -->
        <a href="article.php?id=<?php echo $row['id']; ?>">
          <div class="articles">
            <h4 class="title"><?php echo $row['title']; ?></h4>
            <div class="contents">
              <div class="labels">
                <span class="kind"><?php echo $row['category']; ?></span>
                <span class="time"><?php echo $row['create_date']; ?></span>
                <span class="time">作者: <?php echo $row['name']; ?></span>
              </div>
              <div class="article"><?php echo $row['content_little']; ?></div>
            </div>
          </div>
        </a>
      <?php endforeach; ?>
    <?php else : ?>
      <h4 class="no_articles">尚無文章</h4>
    <?php endif; ?>
  </div>
  <!-- 底部 -->
  <?php 
    require_once 'components/footer.php';
  ?>
</body>

</html>