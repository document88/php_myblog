<!DOCTYPE html>
<html lang="zh-TW">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <!-- 標題選單 -->
  <?php 
    require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main">
      <h4 class="about_index">學習PHP與MySQL與Ajax的應用</h4>
  </div>
  <!-- 底部 -->
  <?php 
    require_once 'components/footer.php';
  ?>
</body>
</html>