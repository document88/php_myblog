<?php
// 載入db.php 讀取數據庫並存入session
require_once 'php/db.php';
// 載入functions.php SQL語句
require_once 'php/functions.php';
// 獲取文章列表
if (isset($_GET['id'])) {
  $get_article = get_article($_GET['id']);
}

?>

<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-<?php echo $get_article['title']; ?></title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 標題選單 -->
  <?php
    require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main">
    <!-- 如果資料庫的文章不為空則渲染 -->
    <?php if (!empty($get_article)) : ?>
      <div class="articles">
        <h4 class="title"><?php echo $get_article['title']; ?></h4>
        <div class="contents">
          <div class="labels">
            <span class="kind"><?php echo $get_article['category']; ?></span>
            <span class="time"><?php echo $get_article['create_date']; ?></span>
            <span class="time">作者: <?php echo $get_article['name']; ?></span>
          </div>
          <?php
            $content = strip_tags($get_article['content'], '<a>');
            $content = htmlspecialchars_decode($content);
            $content = nl2br($content);
            $content = preg_replace('/<a\b/', '<a target="_blank"', $content);
          ?>
          <div class="article"><?php echo $content; ?></div>
        </div>
      </div>
    <?php else : ?>
      <h4 class="no_articles">尚無文章</h4>
    <?php endif; ?>
  </div>
  <!-- 底部 -->
  <?php 
    require_once 'components/footer.php';
  ?>
</body>

</html>