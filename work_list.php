<?php
// 載入db.php 讀取數據庫並存入session
require_once 'php/db.php';
// 載入functions.php SQL語句
require_once 'php/functions.php';
// 獲取作品列表, if搜尋, else預設
if (isset($_GET['search']) && !empty($_GET['search'])){
  $userInput = $_GET['search'];
  $search = mysqli_real_escape_string($_SESSION['link'], $userInput);
  $get_works = get_search_work($search);
}else{
  $get_works = get_publish_works();
}
?>

<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-作品</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 標題選單 -->
  <?php
  require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main">
    <div class="works_box">
      <!-- 如果資料庫的文章不為空則渲染 -->
      <?php if (!empty($get_works)) : ?>
        <?php foreach ($get_works as $row) : ?>
          <!-- php預先處理摘要 -->
          <?php
          //去除所有html標籤
          $abstract = strip_tags($row['title']);
          //取得100個字
          $abstract = mb_substr($abstract, 0, 30, "UTF-8")
          ?>
          <!-- 用$_GET來獲取, ?參=值 -->
          <a class="works_box_flex" href="work.php?id=<?php echo $row['id']; ?>">
            <div class="works">
              <div class="work_left">
                <?php if ($row['image_path']) : ?>
                  <img src='<?php echo $row['image_path']; ?>' class="img-responsive">
                <?php else : ?>
                  <video src="<?php echo $row['video_path']; ?>" controls></video>
                <?php endif; ?>
              </div>
              <div class="contents">
                <h3 class="abstract"><?php echo $abstract; ?></h3>
                <span class="time"><?php echo $row['upload_date']; ?> / 作者: <?php echo $row['name']; ?></span>
              </div>
            </div>
          </a>
        <?php endforeach; ?>
      <?php else : ?>
        <h4 class="no_works">尚無作品</h4>
      <?php endif; ?>
    </div>
  </div>
  <!-- 底部 -->
  <?php
  require_once 'components/footer.php';
  ?>
</body>

</html>