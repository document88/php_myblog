-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 
-- 伺服器版本： 10.4.6-MariaDB
-- PHP 版本： 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `my_db`
--

-- --------------------------------------------------------

--
-- 資料表結構 `article`
--

CREATE TABLE `article` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '文章 id',
  `title` varchar(30) NOT NULL DEFAULT '' COMMENT '標題',
  `category` varchar(50) NOT NULL DEFAULT '' COMMENT '分類',
  `content` text NOT NULL COMMENT '內文',
  `content_little` text NOT NULL COMMENT '縮短內文',
  `publish` tinyint(1) NOT NULL COMMENT '是否發布',
  `create_date` datetime NOT NULL COMMENT '建立日期',
  `modify_date` datetime DEFAULT NULL COMMENT '修改日期',
  `creater_id` int(11) DEFAULT NULL COMMENT '建立者id',
  `name` text NOT NULL COMMENT '暱稱'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `article`
--

INSERT INTO `article` (`id`, `title`, `category`, `content`, `content_little`, `publish`, `create_date`, `modify_date`, `creater_id`, `name`) VALUES
(39, 'Vue2的學習應用', '心得', '<a href=\"https://gitlab.com/document88/Vue2-shoppingCart.git\">https://gitlab.com/document88/Vue2-shoppingCart.git</a>', 'https://gitlab.com/document88/Vue2-shoppingCart.git', 1, '2024-01-19 13:45:45', '2024-01-20 07:49:58', 21, '七七'),
(40, 'php與sql的學習應用', '心得', '<a href=\"https://gitlab.com/document88/php_myblog.git\">https://gitlab.com/document88/php_myblog.git<a>', 'https://gitlab.com/document88/php_myblog.git', 1, '2024-01-19 14:25:58', '2024-01-20 07:50:15', 22, '小六'),
(41, 'css的學習應用', '日記', '&lt;a href=&quot;https://gitlab.com/document88/css_goldfish.git&quot;&gt;https://gitlab.com/document88/css_goldfish.git&lt;a&gt;', 'https://gitlab.com/document88/css_goldfish.git', 1, '2024-01-19 21:27:36', '2024-01-20 08:08:55', 23, '豪');

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL COMMENT '使用者id',
  `username` varchar(30) NOT NULL COMMENT '登⼊帳號',
  `password` varchar(100) NOT NULL COMMENT '使用者密碼',
  `name` varchar(30) NOT NULL COMMENT '名字'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `name`) VALUES
(21, '7', '8f14e45fceea167a5a36dedd4bea2543', '七七'),
(22, '6', '1679091c5a880faf6fb5e6087eb1b2dc', '小六'),
(23, '8', 'c9f0f895fb98ab9159f51fd0297e236d', '豪');

-- --------------------------------------------------------

--
-- 資料表結構 `works`
--

CREATE TABLE `works` (
  `id` int(11) NOT NULL COMMENT '作品 id',
  `title` text NOT NULL COMMENT '名稱',
  `intro` text NOT NULL COMMENT '簡介',
  `image_path` text DEFAULT NULL COMMENT '圖⽚路徑',
  `video_path` text DEFAULT NULL COMMENT '影⽚路徑',
  `publish` tinyint(1) NOT NULL COMMENT '是否發布',
  `upload_date` datetime NOT NULL COMMENT '上傳時間',
  `create_user_id` int(11) NOT NULL COMMENT '誰上傳的(建⽴立者id)',
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `works`
--

INSERT INTO `works` (`id`, `title`, `intro`, `image_path`, `video_path`, `publish`, `upload_date`, `create_user_id`, `name`) VALUES
(17, '多喝熱水', '多喝熱水~多喝熱水~多喝熱水~', 'files/images/1705643153295_熊裝貓寶04.png', NULL, 1, '2024-01-19 13:46:15', 21, '七七'),
(18, '蝦毀', '0.0', 'files/images/1705645565414_貓寶媽咪11.png', NULL, 1, '2024-01-19 14:26:31', 22, '小六'),
(19, '柿', '色鉛筆手繪', 'files/images/1705667337135_柿.jpg', NULL, 1, '2024-01-19 20:30:23', 23, '豪'),
(20, '仿名畫_枯萎', '麥克筆手繪', 'files/images/1705667428580_仿名畫_枯萎.jpg', NULL, 1, '2024-01-19 20:31:31', 23, '豪'),
(21, '如何讓地瓜球變得更美味', '電腦繪圖, 崩壞3美食不打烊繪圖比賽優勝', 'files/images/1705667494664_崩壞3美食不打烊_更美味的地瓜球.jpg', NULL, 1, '2024-01-19 20:34:29', 23, '豪'),
(22, '雙重人格的少女', '電腦繪圖, 兩倍的熱量!!', 'files/images/1705667681995_世界上的另一個我.jpg', NULL, 1, '2024-01-19 20:40:58', 23, '豪'),
(23, '早安', '早安', 'files/images/1705668226556_熊裝貓寶01.png', NULL, 1, '2024-01-19 20:44:23', 23, '豪'),
(24, '午安', '午安', 'files/images/1705668266503_熊裝貓寶02.png', NULL, 1, '2024-01-19 20:44:42', 23, '豪'),
(25, '晚安', '晚安', 'files/images/1705668285842_熊裝貓寶03.png', NULL, 1, '2024-01-19 20:44:56', 23, '豪');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `works`
--
ALTER TABLE `works`
  ADD PRIMARY KEY (`id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '文章 id', AUTO_INCREMENT=51;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '使用者id', AUTO_INCREMENT=24;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `works`
--
ALTER TABLE `works`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '作品 id', AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
