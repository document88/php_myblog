<?php
//啟動 session_start();
@session_start();
// 取得當前路徑
$current_file = $_SERVER['PHP_SELF'];
// 透過當前路徑取得檔案名稱, 去掉.php
$current_file = basename($current_file, ".php");
switch ($current_file) {
    // 為文章列表或完整文章頁1
  case 'article_list':
  case 'article':
    $index = 1;
    break;
    // 為作品列表或完整作品頁2
  case 'work_list':
  case 'work':
    $index = 2;
    break;
    // 為關於我頁3
  case 'about':
    $index = 3;
    break;
    // 為註冊頁4
  case 'register':
    $index = 4;
    break;
    // 預設索引為 0
  default:
    $index = 0;
    break;
}
?>

<div class="top">
  <!-- 大標與主功能 -->
  <div class="one">
    <div class="title">PHP與資料庫<i class="fa-solid fa-wand-magic-sparkles"></i></div>
    <!-- 搜尋文章 -->
    <form class="search" method="get" action="<?php echo ($index === 2) ? 'work_list.php' : 'article_list.php'; ?>">
      <input type="text" name="search" maxlength="30" placeholder="<?php echo ($index === 2) ? '搜尋作品' : '搜尋文章'; ?>" required>
      <button type="submit">
        <i class="fa-solid fa-magnifying-glass"></i>
      </button>
    </form>
    <div class="right">
      <!-- 搜尋文章phone -->
      <div class="search2">
        <label for="search2_checkbox" class="searchi">
          <i class="searchi fa-solid fa-magnifying-glass"></i>
        </label>
        <input type="checkbox" id="search2_checkbox">
        <form class="search_from" method="get" action="<?php echo ($index === 2) ? 'work_list.php' : 'article_list.php'; ?>">
          <input type="text" name="search" maxlength="30" placeholder="<?php echo ($index === 2) ? '搜尋作品' : '搜尋文章'; ?>" required>
          <label for="search2_checkbox">
            <i class="fa-regular fa-circle-xmark"></i>
          </label>
          <button type="submit">
            <i class="fa-solid fa-magnifying-glass"></i>
          </button>
        </form>
      </div>
      <a href="admin/login.php">
        <div class="user"><span><?php echo (isset($_SESSION['is_login']) && $_SESSION['is_login']) ? '後台' : '登入'; ?></span></div>
      </a>
    </div>
  </div>
  <!-- 分頁項目 -->
  <div class="menu">
    <ul class="window">
      <li <?php echo ($index === 0) ? 'class="active"' : ''; ?>><a href="index.php">首頁</a></li>
      <li <?php echo ($index === 1) ? 'class="active"' : ''; ?>><a href="article_list.php">文章</a></li>
      <li <?php echo ($index === 2) ? 'class="active"' : ''; ?>><a href="work_list.php">作品</a></li>
      <li <?php echo ($index === 3) ? 'class="active"' : ''; ?>><a href="about.php">關於我</a></li>
    </ul>
    <div class="minphone">
      <ul class="fixed_bottom">
        <li <?php echo ($index === 0) ? 'class="active"' : ''; ?>><a href="index.php"><i class="fa-solid fa-house"></i>首頁</a></li>
        <li <?php echo ($index === 1) ? 'class="active"' : ''; ?>><a href="article_list.php"><i class="fa-solid fa-pen"></i>文章</a></li>
        <li <?php echo ($index === 2) ? 'class="active"' : ''; ?>><a href="work_list.php"><i class="fa-regular fa-image"></i>作品</a></li>
        <li <?php echo ($index === 3) ? 'class="active"' : ''; ?>><a href="about.php"><i class="fa-regular fa-address-card"></i>關於我</a></li>
      </ul>
    </div>
  </div>
</div>