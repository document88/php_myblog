<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-註冊</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 標題選單 -->
  <?php
  require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main">
    <div class="content">
      <div class="container register_container">
        <div class="row">
          <div class="col-xs-12">
            <form id="signup_form" class="signup_form" action="" method="get">
              <div class="input-group mb-3 signup_inputs">
                <label for="username"><span class="input-group-text" id="basic-addon1">帳號</span></label>
                <input type="text" id="username" class="form-control" name="username" autofocus maxlength="30" required placeholder="請輸入帳號" aria-describedby="basic-addon1">
                <div class="valid-feedback">
                  帳號可以使用
                </div>
                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                  很遺憾帳號已重複或存在禁止的字符
                </div>
              </div>
              <div class="input-group mb-3 signup_inputs">
                <label for="password"><span class="input-group-text" id="basic-addon2">密碼</span></label>
                <input type="password" id="password" class="form-control" name="password" maxlength="30" required placeholder="請輸入密碼" aria-describedby="basic-addon2">
                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                  很遺憾此密碼存在禁止的字符
                </div>
              </div>
              <div class="input-group mb-3 signup_inputs">
                <label for="confirm_password"><span class="input-group-text" id="basic-addon3">確認密碼</span></label>
                <input type="password" id="confirm_password" class="form-control" maxlength="30" required name="confirm_password" placeholder="請再次輸入密碼" aria-describedby="basic-addon3">
                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                  請輸入相同的密碼喔
                </div>
              </div>
              <div class="input-group mb-3 signup_inputs">
                <label for="name"><span class="input-group-text" id="basic-addon4">暱稱</span></label>
                <input type="text" id="name" class="form-control" name="name" maxlength="20" required placeholder="請輸入您的暱稱" aria-describedby="basic-addon4">
              </div>
              <button type="submit" class="btn btn-primary">註冊</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- 底部 -->
  <?php
  require_once 'components/footer.php';
  ?>
  <script src="js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script>
    window.addEventListener('DOMContentLoaded', function() {
      // 確認表單帳號
      var username = document.getElementById("username");
      var delayTimer;
      var usernameOk = true;
      username.addEventListener('input', function(event) {
        // 清除之前的定時器
        clearTimeout(delayTimer);
        // 監聽到事件後開始計時, 再次監聽則重新計時
        delayTimer = setTimeout(function() {
          event.stopPropagation();
          event.target.value = event.target.value.trim()
          if (event.target.value !== '') {
            //$.ajax 是 jQuery 的方法，裡面使用的是物件。
            $.ajax({
              type: "POST", //表單傳送的方式 同 form 的 method 屬性
              url: "php/check_username.php", //目標給哪個檔案 同 form 的 action 屬性
              data: { //為要傳過去的資料，使用物件方式呈現，因為變數key值為英文的關係，所以用物件方式送。ex: {name : "輸入的名字", password : "輸入的密碼"}
                n: $("#username").val() //代表要傳一個 n 變數值為，username 文字方塊裡的值
              },
              dataType: 'html' //設定該網頁回應的會是 html 格式
            }).done(function(data) {
              //成功的時候
              // console.log(data); //透過 console 看回傳的結果
              if (data == 'yes' && !(/[^a-zA-Z0-9]/.test(username.value)) && username.value !== '') {
                // 移除 is-invalid 類別
                username.classList.remove("is-invalid");
                // 檢查是否已經有 is-valid 類別，若沒有則添加
                if (!username.classList.contains("is-valid")) {
                  username.classList.add("is-valid");
                }
                usernameOk = true
                $('#signup_form button[type="submit"]').removeClass('disabled')
              } else {
                usernameOk = false
                $('#signup_form button[type="submit"]').addClass('disabled')
                // 移除 is-valid 類別
                username.classList.remove("is-valid");
                // 檢查是否已經有 is-invalid 類別，若沒有則添加
                if (!username.classList.contains("is-invalid")) {
                  username.classList.add("is-invalid");
                }
              }
            }).fail(function(jqXHR, textStatus, errorThrown) {
              //失敗的時候
              alert("有錯誤產生，請看 console log");
              console.log(jqXHR.responseText);
            });
          } else {
            // 如果為空 移除 is-valid is-invalid 類別
            username.classList.remove("is-valid");
            username.classList.remove("is-invalid");
            usernameOk = true
            $('#signup_form button[type="submit"]').removeClass('disabled')
          }
        }, 100);
      })
      // 確認表單密碼
      var passwordInput = document.getElementById("password");
      var delayTimer2;
      passwordInput.addEventListener('input', function(event) {
        // 清除之前的定時器
        clearTimeout(delayTimer2);
        // 監聽到事件後開始計時, 再次監聽則重新計時
        delayTimer2 = setTimeout(function() {
          event.stopPropagation();
          event.target.value = event.target.value.trim()
          if (event.target.value !== '') {
            if (!(/[^a-zA-Z0-9]/.test(passwordInput.value)) && passwordInput.value !== '') {
              // 移除 is-invalid出錯提示 類別
              passwordInput.classList.remove("is-invalid");
              if (usernameOk === true) {
              $('#signup_form button[type="submit"]').removeClass('disabled')
              }
            } else {
              $('#signup_form button[type="submit"]').addClass('disabled')
              // 檢查是否已經有 is-invalid出 類別，若沒有則添加
              if (!passwordInput.classList.contains("is-invalid")) {
                passwordInput.classList.add("is-invalid");
              }
            }
          } else {
            // 如果為空 移除 is-valid is-invalid 類別
            passwordInput.classList.remove("is-invalid");
            if (usernameOk === true) {
            $('#signup_form button[type="submit"]').removeClass('disabled')
            }
          }
        }, 100);
      })
      // 確認表單'確認密碼框'
      var signup_form = document.getElementById("signup_form");
      var confirmPasswordInput = document.getElementById("confirm_password");
      var nameInput = document.getElementById("name");
      signup_form.addEventListener('submit', function(event) {
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("confirm_password").value;
        // 去頭尾空格
        username.value = username.value.trim();
        passwordInput.value = passwordInput.value.trim()
        nameInput.value = nameInput.value.trim();
        if (username.value === "" || passwordInput.value === "" || nameInput.value === "") {
          event.preventDefault(); // 阻止表单提交
          return alert("輸入不能為空");
        } else {
          // 檢查密碼是否相同
          if (password !== confirmPassword) {
            // 如果密碼不匹配，新增 is-invalid class
            confirmPasswordInput.classList.add("is-invalid");
          } else {
            // 如果密碼匹配，移除 is-invalid class
            confirmPasswordInput.classList.remove("is-invalid");
            // ajax傳送表單
            $.ajax({
              type: "POST",
              url: "php/add_user.php",
              data: {
                un: $("#username").val(), //使用者帳號
                pw: $("#password").val(), //使用者密碼
                n: $("#name").val() //匿名
              },
              dataType: 'html' //設定該網頁回應的會是 html 格式
            }).done(function(data) {
              console.log(data);
              if (data == 'yes') {
                alert('註冊成功')
                window.location.href = "admin/index.php";
              } else {
                alert('註冊失敗')
              }
            }).fail(function(jqXHR, textStatus, errorThrown) {
              //失敗的時候
              alert("有錯誤產生，請看 console log");
              console.log(jqXHR.responseText);
            });
          }
        }
        // 阻止表單提交
        event.preventDefault();
      })
      // 確認密碼框重新輸入時，移除 is-invalid class
      confirmPasswordInput.addEventListener('input', function(event) {
        confirmPasswordInput.classList.remove("is-invalid");
      })
    })
  </script>
</body>

</html>