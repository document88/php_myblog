<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-關於我</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 標題選單 -->
  <?php 
    require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main">
    <div class="about">
      <div class="about_me">
        <img id="apngImage" src="images/about/01.png" alt="APNG Image" onclick="restartAPNG()">
        <div class="text">
          <h3>姓名: 楊文豪</h3>
          <h3>信箱: s355164s@gmail.com</h3>
          <h3>專長: 電腦繪圖, 網頁前端</h3>
          <h3>學歷: 龍華科技大學</h3>
        </div>
      </div>
      <div class="about_work">
        <div class="title">履歷</div>
        <div class="text">
          我叫楊文豪，<br>
          畢業於龍華科技大學多媒體與遊戲發展科學系，<br>
          擅長電腦繪圖，<br>
          我較常使用平塗技法繪製人物和Q版腳色。<br>
          我熟悉使用 Photoshop 和 Illustrator 等繪圖軟體，<br>
          並擁有排版技能的相關證照，<br>
          這些技能不僅讓我有創作與設計的能力，<br>
          也讓我對視覺表現方面有更高的敏銳度。<br>
          <br>
          除了繪圖，我也學習網頁前端開發，<br>
          具備前端開發所需要的 HTML、CSS 和 JavaScript 等基礎技能，<br>
          運用 Vue 框架建構完整的網頁前端架構。<br>
          <br>
          畢業後，做過印刷廠的品管，<br>
          因為我媽媽是一位保母，<br>
          我在離職後便幫忙照顧親戚的小孩，<br>
          在照顧小孩的空閒期間也參加過自來水與郵局的考試，<br>
          雖然總是差了一點沒考上，<br>
          但這段經歷培養了我對失敗的抗壓性和持續努力的態度。<br>
          <br>
          平時為了提升自己的技能，<br>
          我也自學電腦繪圖和網頁前端開發。
        </div>
      </div>
    </div>
  </div>
  <!-- 底部 -->
  <?php 
    require_once 'components/footer.php';
  ?>
  <script>
    function restartAPNG() {
      var apngImage = document.getElementById('apngImage');
      apngImage.src = ''; // 清空src
      apngImage.src = 'images/about/01.png'; // 重新設置src，觸發重新加載
    }
  </script>
</body>

</html>