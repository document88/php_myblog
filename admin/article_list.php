<?php
//載入 db.php 檔案, 啟用session與資料庫
require_once '../php/db.php';
// 載入數據庫SQL語句
require_once '../php/functions.php';
// 如果沒登入
if (!isset($_SESSION['is_login']) || !$_SESSION['is_login']) {
  //直接轉跳到登入頁面
  header("Location: login.php");
}

// 獲取文章列表
if (isset($_GET['search']) && !empty($_GET['search'])) {
  $userInput = $_GET['search'];
  $search = mysqli_real_escape_string($_SESSION['link'], $userInput);
  $articles = admin_get_search_article($_SESSION['login_user_id'], $search);
} else {
  //取得所有文章
  $articles = get_all_article($_SESSION['login_user_id']);
}
?>
<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-後台-文章</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 標題選單 -->
  <?php
  require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main article_list_main">
    <!-- 網站內容 -->
    <div class="article_list content">
      <div class="container">
        <!-- 建立第一個 row 空間，裡面準備放格線系統 -->
        <div class="row">
          <!-- 在 xs 尺寸，佔12格，可參考 http://getbootstrap.com/css/#grid 說明-->
          <div class="col-xs-12">
            <a href='article_add.php'>
              <button type="button" class="list_title btn btn-primary">新增文章</button>
            </a>
          </div>
          <div class="col-xs-12">
            <!-- 資料列表 -->
            <table class="table table-striped table-hover article_list_table">
              <thead>
                <tr>
                  <th>分類</th>
                  <th>標題</th>
                  <th>發布狀況</th>
                  <th>上傳時間</th>
                  <th>管理動作</th>
                </tr>
              </thead>
              <?php if ($articles) : ?>
                <?php foreach ($articles as $article) : ?>
                  <tr>
                    <td><?php echo $article['category']; ?></td>
                    <td><?php echo $article['title']; ?></td>
                    <td><?php echo ($article['publish']) ? "發布" : "下架中"; ?></td>
                    <td><?php echo $article['create_date']; ?></td>
                    <td>
                      <a href='article_edit.php?id=<?php echo $article['id']; ?>' class="btn btn-success">編輯</a>
                      <a href='javascript:void(0);' class='btn btn-danger del_article' data-id="<?php echo $article['id']; ?>">刪除</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php else : ?>
                <tr>
                  <td colspan="5">無資料</td>
                </tr>
              <?php endif; ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- 底部 -->
  <?php
  require_once 'components/footer.php';
  ?>
  <script src="../js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script>
    $(function() {
      //表單送出
      $("a.del_article").on("click", function() {
        //宣告變數 確認框
        var c = confirm("您確定要刪除嗎？");
        this_tr = $(this).parent().parent();
        if (c) {
          $.ajax({
            type: "POST",
            url: "../php/del_article.php", //因為此檔案是放在 admin 資料夾內，若要前往 php，就要回上一層 ../ 找到 php 才能進入 add_article.php
            data: {
              id: $(this).attr("data-id") //文章id
            },
            dataType: 'html' //設定該網頁回應的會是 html 格式
          }).done(function(data) {
            //成功的時候

            if (data == "yes") {
              //註冊新增成功，轉跳到登入頁面。
              alert("刪除成功，點擊確認從列表移除");
              this_tr.fadeOut('slow', function() {
                location.reload();
              });
            } else {
              alert("刪除錯誤:" + data);
            }

          }).fail(function(jqXHR, textStatus, errorThrown) {
            //失敗的時候
            alert("有錯誤產生，請看 console log");
            console.log(jqXHR.responseText);
          });
        }
        return false;
      });
    });
  </script>
</body>

</html>