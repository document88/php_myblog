<?php
// 取得當前路徑
$current_file = $_SERVER['PHP_SELF'];
// 透過當前路徑取得檔案名稱, 去掉.php
$current_file = basename($current_file, ".php");
switch ($current_file) {
    // 文章管理頁1
  case 'article_list':
  case 'article_edit':
  case 'article_add':
    $index = 1;
    break;
    // 作品管理頁2
  case 'work_list':
  case 'work_edit':
  case 'work_add':
    $index = 2;
    break;
    // 使用者管理頁3
  case 'member_list':
  case 'member_edit':
  case 'member_add':
    $index = 3;
    break;
    // 預設索引為 0
  default:
    $index = 0;
    break;
}
?>

<div class="top">
  <!-- 大標與主功能 -->
  <div class="one">
    <div class="title">PHP與資料庫-後台<i class="fa-solid fa-wand-magic-sparkles"></i></div>
    <!-- 搜尋文章 -->
    <form class="search" method="get" action="<?php echo ($index === 2) ? 'work_list.php' : 'article_list.php'; ?>">
      <input type="text" name="search" maxlength="30" placeholder="<?php echo ($index === 2) ? '搜尋後台作品' : '搜尋後台文章'; ?>" required>
      <button type="submit">
        <i class="fa-solid fa-magnifying-glass"></i>
      </button>
    </form>
    <div class="right">
      <!-- 搜尋文章phone -->
      <div class="search2">
        <label for="search2_checkbox" class="searchi">
          <i class="searchi fa-solid fa-magnifying-glass"></i>
        </label>
        <input type="checkbox" id="search2_checkbox">
        <form class="search_from" method="get" action="<?php echo ($index === 2) ? 'work_list.php' : 'article_list.php'; ?>">
          <input type="text" name="search" maxlength="30" placeholder="<?php echo ($index === 2) ? '搜尋後台作品' : '搜尋後台文章'; ?>" required>
          <label for="search2_checkbox">
            <i class="fa-regular fa-circle-xmark"></i>
          </label>
          <button type="submit">
            <i class="fa-solid fa-magnifying-glass"></i>
          </button>
        </form>
      </div>
      <a href="member_list.php">
        <!-- 處理name -->
      <?php
        //去除所有html標籤
        $user_name = strip_tags($_SESSION['login_user_name']);
        //取得1個字
        $user_name = mb_substr($user_name, 0, 1, "UTF-8")
        ?>
        <div class="user<?php echo ($index === 3) ? ' member_class' : ''; ?>"><span><?php echo "{$user_name}"?></span></div>
      </a>
    </div>
  </div>
  <!-- 分頁項目 -->
  <div class="menu">
    <ul class="window">
      <li><a href="../">前台首頁</a></li>
      <li <?php echo ($index == 0) ? 'class="active"' : ''; ?>><a href="./">後台首頁</a></li>
      <li <?php echo ($index == 1) ? 'class="active"' : ''; ?>><a href="article_list.php">文章管理</a></li>
      <li <?php echo ($index == 2) ? 'class="active"' : ''; ?>><a href="work_list.php">作品管理</a></li>
      <li><a href="../php/logout.php">登出</a></li>
    </ul>
    <div class="minphone">
      <ul class="fixed_bottom">
        <li><a href="../"><i class="fa-solid fa-house"></i>前台首頁</a></li>
        <li <?php echo ($index === 0) ? 'class="active"' : ''; ?>><a href="./"><i class="fa-solid fa-computer"></i>後台首頁</a></li>
        <li <?php echo ($index === 1) ? 'class="active"' : ''; ?>><a href="article_list.php"><i class="fa-solid fa-pen"></i>文章管理</a></li>
        <li <?php echo ($index === 2) ? 'class="active"' : ''; ?>><a href="work_list.php"><i class="fa-regular fa-image"></i>作品管理</a></li>
        <li><a href="../php/logout.php"><i class="fa-solid fa-right-from-bracket"></i>登出</a></li>
      </ul>
    </div>
  </div>
</div>