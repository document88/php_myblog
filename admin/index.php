<?php
//載入 db.php 檔案, 啟用session與資料庫
require_once '../php/db.php';
// 如果沒登入
if(!isset($_SESSION['is_login']) || !$_SESSION['is_login'])
{
	//直接轉跳到登入頁面
	header("Location: login.php");
}
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-後台</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <!-- 標題選單 -->
  <?php 
    require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
     <!-- 處理name -->
     <?php
        //去除所有html標籤
        $user_name = strip_tags($_SESSION['login_user_name']);
        ?>
  <div class="main">
      <h4 class="about_index"><?php echo "{$user_name}你好，這裡是後台";?></h4>
  </div>
  <!-- 底部 -->
  <?php 
    require_once 'components/footer.php';
  ?>
</body>
</html>