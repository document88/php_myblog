<?php
//載入 db.php 檔案, 啟用session與資料庫
require_once '../php/db.php';
// 載入數據庫SQL語句
require_once '../php/functions.php';
// 如果沒登入
if (!isset($_SESSION['is_login']) || !$_SESSION['is_login']) {
  //直接轉跳到登入頁面
  header("Location: login.php");
}
//如果個人訊息是null 登出
if(is_null($_SESSION['login_user_id']) || is_null($_SESSION['login_user_username']) || is_null($_SESSION['login_user_name']))
{
	header("Location: ../php/logout.php");
}
// 更新數據
admin_member_user($_SESSION['login_user_id'])

?>
<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-後台-個人訊息</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 標題選單 -->
  <?php
  require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main">
    <!-- 網站內容 -->
    <div class="member_list content">
      <div class="container">
        <!-- 建立第一個 row 空間，裡面準備放格線系統 -->
        <div class="row">
          <!-- 在 xs 尺寸，佔12格，可參考 http://getbootstrap.com/css/#grid 說明-->
          <div class="col-xs-12">
            <form id="add_article_form">
            <h2 class="title">個人資訊</h2>
              <div>會員ID: <?php echo $_SESSION['login_user_id'];?></div>
              <div class="form-group">
                <label for="username">帳號</label>
                <input readonly type="input" class="form-control" id="username" value="<?php echo $_SESSION['login_user_username'];?>">
              </div>
              <div class="form-group">
                <label for="name">暱稱</label>
                <input readonly type="input" class="form-control" id="name" value="<?php echo $_SESSION['login_user_name'];?>">
              </div>
              <div class="submit_box">
              <a href='member_edit.php' class="btn btn-success">編輯個人資訊</a>
              <a href='javascript:void(0);' class='btn btn-danger del_member' data-id="<?php echo $_SESSION['login_user_id']; ?>">刪除會員帳號</a>
              </div> 
              <div class="loading text-center"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- 底部 -->
  <?php
  require_once 'components/footer.php';
  ?>
  <script src="../js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script>
    $(function() {
      //表單送出
      $("a.del_member").on("click", function(){
    			//宣告變數
    			var c = confirm("您確定要刪除此會員帳號嗎？")
    			if(c)
    			{
            var c2 = confirm("您真的真的確定要刪除此會員帳號嗎？")
            if(!c2){
              return
            }
    				$.ajax({
            type : "POST",
            url : "../php/del_member.php", //因為此檔案是放在 admin 資料夾內，若要前往 php，就要回上一層 ../ 找到 php 才能進入 add_article.php
            data : {
              id : $(this).attr("data-id") //使用者id
            },
            dataType : 'html' //設定該網頁回應的會是 html 格式
          }).done(function(data) {
            //成功的時候
            
            if(data == "yes")
            {
              alert("刪除成功");
              window.location.href = '../php/logout.php';
            }
            else
            {
              alert("刪除錯誤:"+data);
            }
            
          }).fail(function(jqXHR, textStatus, errorThrown) {
            //失敗的時候
            alert("有錯誤產生，請看 console log");
            console.log(jqXHR.responseText);
          });
    			}
          
    			return false;
    		});
    });
  </script>
</body>

</html>