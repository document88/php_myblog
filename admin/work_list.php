<?php
//載入 db.php 檔案，讓我們可以透過它連接資料庫，另外後台都會用 session 判別暫存資料，所以要請求 db.php 因為該檔案最上方有啟動session_start()。
require_once '../php/db.php';
require_once '../php/functions.php';
//print_r($_SESSION); //查看目前session內容

//如過沒有 $_SESSION['is_login'] 這個值，或者 $_SESSION['is_login'] 為 false 都代表沒登入
if (!isset($_SESSION['is_login']) || !$_SESSION['is_login']) {
  //直接轉跳到 login.php
  header("Location: login.php");
}

// 獲取搜尋的作品列表
if (isset($_GET['search']) && !empty($_GET['search'])) {
  $userInput = $_GET['search'];
  $search = mysqli_real_escape_string($_SESSION['link'], $userInput);
  $works = get_work_id_search($_SESSION['login_user_id'], $search);
} else {
  //取得所有作品
  $works = get_userid_all_works($_SESSION['login_user_id']);
}

?>

<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-後台-作品</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 標題選單 -->
  <?php
  require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main article_list_main admin_work_list">
    <!-- 網站內容 -->
    <div class="article_list content">
      <div class="container">
        <!-- 建立第一個 row 空間，裡面準備放格線系統 -->
        <div class="row">
          <!-- 在 xs 尺寸，佔12格，可參考 http://getbootstrap.com/css/#grid 說明-->
          <div class="col-xs-12">
            <a href='work_add.php'>
              <button type="button" class="list_title btn btn-primary">新增作品</button>
            </a>
          </div>
          <div class="col-xs-12">
            <!-- 資料列表 -->
            <table class="table table-striped table-hover article_list_table">
              <thead>
                <tr>
                  <th>名稱</th>
                  <th>作品路徑</th>
                  <th>發佈狀況</th>
                  <th>管理動作</th>
                </tr>
              </thead>
              <?php if ($works) : ?>
                <?php foreach ($works as $work) : ?>
                  <tr>
                    <?php
                    //去除所有html標籤
                    $title = strip_tags($work['title']);
                    //取得1個字
                    $title = mb_substr($title, 0, 10, "UTF-8")
                    ?>
                    <td><?php echo $title ?></td>
                    <td><?php echo ($work['video_path']) ? $work['video_path'] : $work['image_path']; ?></td>
                    <td><?php echo ($work['publish']) ? "發布" : "下架中"; ?></td>
                    <td>
                      <a href='work_edit.php?id=<?php echo $work['id']; ?>' class="btn btn-success">編輯</a>
                      <a href='javascript:void(0);' class='btn btn-danger del_work' data-id="<?php echo $work['id']; ?>">刪除</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php else : ?>
                <tr>
                  <td colspan="5">無資料</td>
                </tr>
              <?php endif; ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- 底部 -->
  <?php
  require_once 'components/footer.php';
  ?>
  <script src="../js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script>
    $(function() {
      //表單送出
      //表單送出
      $("a.del_work").on("click", function() {
        //宣告變數
        var c = confirm("您確定要刪除嗎？"),
          this_tr = $(this).parent().parent();
        if (c) {
          $.ajax({
            type: "POST",
            url: "../php/del_work.php", //因為此檔案是放在 admin 資料夾內，若要前往 php，就要回上一層 ../ 找到 php 才能進入 add_article.php
            data: {
              id: $(this).attr("data-id") //作品id
            },
            dataType: 'html' //設定該網頁回應的會是 html 格式
          }).done(function(data) {
            //成功的時候
            if (data == "yes") {
              alert("刪除成功，點擊確認從列表移除");
              this_tr.fadeOut('slow', function() {
                location.reload();
              });
            } else {
              alert("刪除錯誤:" + data);
            }
          }).fail(function(jqXHR, textStatus, errorThrown) {
            //失敗的時候
            alert("有錯誤產生，請看 console log");
            console.log(jqXHR.responseText);
          });
        }
        return false;
      });
    });
  </script>
</body>

</html>