<?php
//載入 db.php 檔案, 啟用session與資料庫
require_once '../php/db.php';
// 載入數據庫SQL語句
require_once '../php/functions.php';
// 如果沒登入
if (!isset($_SESSION['is_login']) || !$_SESSION['is_login']) {
  //直接轉跳到登入頁面
  header("Location: login.php");
}

//如果個人訊息是null 登出
if (is_null($_SESSION['login_user_id']) || is_null($_SESSION['login_user_username']) || is_null($_SESSION['login_user_name'])) {
  header("Location: ../php/logout.php");
}

?>
<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-後台-編輯個人訊息</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 標題選單 -->
  <?php
  require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main">
    <!-- 網站內容 -->
    <div class="member_list content">
      <div class="container">
        <!-- 建立第一個 row 空間，裡面準備放格線系統 -->
        <div class="row">
          <!-- 在 xs 尺寸，佔12格，可參考 http://getbootstrap.com/css/#grid 說明-->
          <div class="col-xs-12">
            <form id="edit_user_form">
              <h2 class="title">編輯個人資訊</h2>
              <div>會員ID: <?php echo $_SESSION['login_user_id']; ?></div>
              <div class="form-group">
                <label for="username">會員帳號</label>
                <input readonly type="input" class="form-control" id="username" value="<?php echo $_SESSION['login_user_username']; ?>">
              </div>
              <div class="form-group">
                <label for="password">修改密碼</label>
                <input type="password" class="form-control" id="password" placeholder="不修改密碼則不填寫此欄位">
                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                  很遺憾此密碼存在禁止的字符
                </div>
              </div>
              <div class="form-group">
                <label for="name">修改暱稱</label>
                <input required type="input" class="form-control" id="name" value="<?php echo $_SESSION['login_user_name']; ?>">
              </div>
              <div class="submit_box">
                <button type="submit" class="btn btn-primary submit">送出修改</button>
              </div>
              <div class="loading text-center"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- 底部 -->
  <?php
  require_once 'components/footer.php';
  ?>
  <script src="../js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script>
    $(function() {
      // 獲取ID
      var userId = <?php echo $_SESSION['login_user_id']; ?>
      // 確認表單密碼
      var passwordInput = document.getElementById("password");
      var delayTimer;
      passwordInput.addEventListener('input', function(event) {
        // 清除之前的定時器
        clearTimeout(delayTimer);
        // 監聽到事件後開始計時, 再次監聽則重新計時
        delayTimer = setTimeout(function() {
          event.stopPropagation();
          event.target.value = event.target.value.trim()
          if (event.target.value !== '') {
            if (!(/[^a-zA-Z0-9]/.test(passwordInput.value)) && passwordInput.value !== '') {
              // 移除 is-invalid出錯提示 類別
              passwordInput.classList.remove("is-invalid");
              $('#edit_user_form button[type="submit"]').removeClass('disabled')
            } else {
              $('#edit_user_form button[type="submit"]').addClass('disabled')
              // 檢查是否已經有 is-invalid出 類別，若沒有則添加
              if (!passwordInput.classList.contains("is-invalid")) {
                passwordInput.classList.add("is-invalid");
              }
            }
          } else {
            // 如果為空 移除 is-valid is-invalid 類別
            passwordInput.classList.remove("is-invalid");
            $('#edit_user_form button[type="submit"]').removeClass('disabled')
          }
        }, 100);
      })
      //表單送出
      $("#edit_user_form").on("submit", function(event) {
        // 獲取暱稱
        var name = $("#name").val().trim()
        $("#name").val(name)
        console.log($("#name").val());
        //宣告 send_data 物件變數，先取得值
        var send_data = {
          id: userId,
          name: $("#name").val()
        };

        //如果有輸入密碼，再將密碼加入
        if ($("#password").val() !== '') {
          send_data.password = $("#password").val();
        }

        //加入loading icon
        $("div.loading").html('<div class="spinner-border text-primary" role="status"><span class="visually-hidden">Loading...</span></div>');

        // 暱稱必須填寫
        if (name === "") {
          event.preventDefault();
          alert("請填入暱稱");
          //清掉 loading icon
          $("div.loading").html('');
        } else {
          //使用 ajax 送出 帳密給 verify_user.php
          $.ajax({
            type: "POST",
            url: "../php/update_member.php", //因為此檔案是放在 admin 資料夾內，若要前往 php，就要回上一層 ../ 找到 php 才能進入 add_article.php
            data: send_data,
            dataType: 'html' //設定該網頁回應的會是 html 格式
          }).done(function(data) {
            //成功的時候

            if (data == "yes") {
              //註冊新增成功，轉跳到登入頁面。
              alert("更新成功，點擊確認回列表");
              window.location.href = "member_list.php";
            } else {
              alert("不存在需要更新的數據");
              window.location.href = "member_list.php";
            }

          }).fail(function(jqXHR, textStatus, errorThrown) {
            //失敗的時候
            alert("有錯誤產生，請看 console log");
            console.log(jqXHR.responseText);
          });
        }
        return false;
      });
    });
  </script>
</body>

</html>