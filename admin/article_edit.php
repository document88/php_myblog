<?php
//載入 db.php 檔案, 啟用session與資料庫
require_once '../php/db.php';
// 載入數據庫SQL語句
require_once '../php/functions.php';
// 如果沒登入
if (!isset($_SESSION['is_login']) || !$_SESSION['is_login']) {
  //直接轉跳到登入頁面
  header("Location: login.php");
}
//取得文章資料，從網址上的 id 取得文章id
$data = admin_get_article($_GET['id']);

//如果文章是null 或 創建文章者不是登入的使用者 就轉回列表頁
if(is_null($data) || $_SESSION['login_user_id'] !== $data['creater_id'])
{
	header("Location: article_list.php");
}
?>
<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-後台-編輯文章</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 標題選單 -->
  <?php
  require_once 'components/menu.php';
  ?>
  <!-- 內容 -->
  <div class="main">
    <!-- 網站內容 -->
    <div class="article_list content">
      <div class="container">
        <!-- 建立第一個 row 空間，裡面準備放格線系統 -->
        <div class="row">
          <!-- 在 xs 尺寸，佔12格，可參考 http://getbootstrap.com/css/#grid 說明-->
          <div class="col-xs-12">
            <form id="add_article_form">
              <div>文章ID: <?php echo $data['id'];?></div>
              <div class="form-group">
                <label for="title">標題</label>
                <input type="input" class="form-control" id="title" value="<?php echo $data['title'];?>" maxlength="30" required>
              </div>
              <div class="form-group">
                <label for="category">分類</label>
                <select id="category" class="form-control">
                  <option value="心得" <?php echo ($data['category'] == "心得")?"selected":"";?>>心得</option>
                  <option value="日記" <?php echo ($data['category'] == "日記")?"selected":"";?>>日記</option>
                  <option value="新聞" <?php echo ($data['category'] == "新聞")?"selected":"";?>>新聞</option>
                  <option value="知識" <?php echo ($data['category'] == "知識")?"selected":"";?>>知識</option>
                  <option value="廢文" <?php echo ($data['category'] == "廢文")?"selected":"";?>>廢文</option>
                </select>
              </div>
              <div class="form-group">
                <label for="content">內容</label>
                <textarea type="input" class="form-control" id="content" rows="5" required><?php echo $data['content'];?></textarea>
              </div>
              <div class="form-group">
                <label class="radio-inline">
                  <input type="radio" name="publish" value="1" <?php echo ($data['publish'] == 1)?"checked":"";?>> 發布
                </label>
                <label class="radio-inline">
                  <input type="radio" name="publish" value="0" <?php echo ($data['publish'] == 0)?"checked":"";?>> 不發佈
                </label>
              </div>
              <div class="submit_box">
                <button type="submit" class="btn btn-primary submit">送出修改</button>
              </div> 
              <div class="loading text-center"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- 底部 -->
  <?php
  require_once 'components/footer.php';
  ?>
  <script src="../js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script>
    $(function() {
      // 取得文章ID
      var idValue = <?php echo $data['id']; ?>;
      //表單送出
      $("#add_article_form").on("submit", function() {
        //加入loading icon
        $("div.loading").html('<div class="spinner-border text-primary" role="status"><span class="visually-hidden">Loading...</span></div>');
        if ($("#title").val() == '' || $("#content").val() == '') {
          alert("請填入標題或內文");

          //清掉 loading icon
          $("div.loading").html('');
        } else {
          //使用 ajax 送出 帳密給 verify_user.php
          $.ajax({
            type: "POST",
            url: "../php/updata_article.php", //因為此檔案是放在 admin 資料夾內，若要前往 php，就要回上一層 ../ 找到 php 才能進入 add_article.php
            data: {
              id : idValue, //這篇文章的id
              title: $("#title").val(), //標題
              category: $("#category").val(), //分類
              content: $("#content").val(), //內文
              publish: $("input[name='publish']:checked").val() //發布狀態
            },
            dataType: 'html' //設定該網頁回應的會是 html 格式
          }).done(function(data) {
            //成功的時候

            if (data == "yes") {
              //註冊新增成功，轉跳到登入頁面。
              alert("修改成功，點擊確認回列表");
              window.location.href = "article_list.php";
            } else {
              alert("新增錯誤");
            }

          }).fail(function(jqXHR, textStatus, errorThrown) {
            //失敗的時候
            alert("有錯誤產生，請看 console log");
            console.log(jqXHR.responseText);
          });
        }
        // 阻止表單提交
        event.preventDefault();
        return false;
      });
    });
  </script>
</body>

</html>