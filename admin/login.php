<?php
//載入 db.php 檔案, 啟用session與資料庫
require_once '../php/db.php';
// 如果有登入
if (isset($_SESSION['is_login']) && $_SESSION['is_login']) {
  //直接轉跳到後台頁面
  header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-登入</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 內容 -->
  <div class="main">
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <form id="login_form" class="signup_form" action="" method="get">
              <h2 class="title">會員登入</h2>
              <div class="input-group mb-3 signup_inputs">
                <label for="username"><span class="input-group-text" id="basic-addon1">帳號</span></label>
                <input type="text" id="username" class="form-control" name="username" autofocus maxlength="30" required placeholder="請輸入帳號" aria-describedby="basic-addon1">
              </div>
              <div class="input-group mb-3 signup_inputs">
                <label for="password"><span class="input-group-text" id="basic-addon2">密碼</span></label>
                <input type="password" id="password" class="form-control" name="password" maxlength="30" required placeholder="請輸入密碼" aria-describedby="basic-addon2">
              </div>
              <button type="submit" class="btn btn-primary">登入</button>
              <a class="go_register" href="../register.php">尚未註冊請點這裡</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- 底部 -->
  <?php
  require_once 'components/footer.php';
  ?>
  <script src="../js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script>
    window.addEventListener('DOMContentLoaded', function() {
      // ajax傳送登入表單
      var login_form = document.getElementById("login_form");
      login_form.addEventListener('submit', function(event) {
        // ajax傳送表單
        $.ajax({
          type: "POST",
          url: "../php/verify_user.php",
          data: {
            un: $("#username").val(), //使用者帳號
            pw: $("#password").val(), //使用者密碼
          },
          dataType: 'html' //設定該網頁回應的會是 html 格式
        }).done(function(data) {
          if (data == 'yes') {
            alert('登入成功')
            window.location.href = "index.php";
          } else {
            alert('登入失敗')
          }
        }).fail(function(jqXHR, textStatus, errorThrown) {
          //失敗的時候
          alert("有錯誤產生，請看 console log");
          console.log(jqXHR.responseText);
        });
        // 阻止表單提交
        event.preventDefault();
      })
    })
  </script>
</body>

</html>