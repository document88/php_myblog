<?php
//載入 db.php 檔案，讓我們可以透過它連接資料庫，另外後台都會用 session 判別暫存資料，所以要請求 db.php 因為該檔案最上方有啟動session_start()。
require_once '../php/db.php';
//print_r($_SESSION); //查看目前session內容

//如過沒有 $_SESSION['is_login'] 這個值，或者 $_SESSION['is_login'] 為 false 都代表沒登入
if (!isset($_SESSION['is_login']) || !$_SESSION['is_login']) {
  //直接轉跳到 login.php
  header("Location: login.php");
}
?>
<!DOCTYPE html>
<html lang="zh-TW">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP與資料庫-後台-新增作品</title>
  <meta name="description" content="學習php與mySQL的使用">
  <meta name="author" content="楊文豪">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <!-- 頁首 -->
  <?php
  include_once 'components/menu.php';
  ?>

  <!-- 內容 -->
  <div class="main article_list_main">
    <div class="content">
      <div class="container">
        <!-- 建立第一個 row 空間，裡面準備放格線系統 -->
        <div class="row">
          <!-- 在 xs 尺寸，佔12格，可參考 http://getbootstrap.com/css/#grid 說明-->
          <div class="col-xs-12">
            <form id="add_article_form">
              <div class="form-group">
                <label for="title">標題</label>
                <input type="input" class="form-control" id="title" autofocus maxlength="30" required>
              </div>
              <div class="form-group">
                <label for="intro">簡介 </label>
                <textarea type="input" class="form-control" id="intro" rows="5" required></textarea>
              </div>
              <div class="form-group">
                <label for="category_image">圖片上傳</label>
                <input type="file" id="category_image" name="image_path" accept="image/gif, image/jpeg, image/png">
                <input type="hidden" id="image_path" value="">
                <div class="image_box">
                  <div class="image"></div>
                  <a href='javascript:void(0);' class="del_image btn btn-danger">刪除照片</a>
                </div>

              </div>
              <div class="form-group">
                <label for="category_video">影片上傳</label>
                <input type="file" id="category_video" name="video_path" accept="video/mp4">
                <input type="hidden" id="video_path" value="">
                <div class="video_box">
                  <div class="video"></div>
                  <a href='javascript:void(0);' class="del_video btn btn-danger">刪除影片</a>
                </div>
              </div>
              <div class="form-group">
                <label class="radio-inline">
                  <input type="radio" name="publish" value="1" checked>
                  發布 </label>
                <label class="radio-inline">
                  <input type="radio" name="publish" value="0">
                  不發佈 </label>
              </div>
              <div class="submit_box">
                <button type="submit" class="btn btn-primary">
                  送出
                </button>
              </div>
              <div class="loading text-center"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- 頁底 -->
  <?php
  require_once 'components/footer.php';
  ?>

  <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <script>
    $(document).on("ready", function() {
      // 建立檔案物件
      var file_data = new FormData();
      // 檔案唯一標示
      var time_name = Date.now() + '_';

      /**
       * 圖片上傳
       */
      //上傳圖片的input更動的時候
      $("input[name='image_path']").on("change", function() {
        // 有選檔案才繼續
        if ($("input[name='image_path']").val() === '') {
          //如果有舊圖片路徑，就刪除該檔案
          if ($("#image_path").val() !== '') {
            // 清空图片预览
            $("div.image").html('');
            // 清空存檔路徑
            $("#image_path").val('');
            // 重置文件输入框
            $('#category_image').val('')
            // 隱藏
            $("#add_article_form .image_box").hide();
            // 啟用input
            $("input[name='video_path']").prop("disabled", false);
          }
          return
        }

        //產生 FormData 物件
        var file_name = $(this)[0].files[0]['name'];
        var save_path = "files/images/";
        //FormData 新增剛剛選擇的檔案
        file_data.append("file", $(this)[0].files[0]);
        file_data.append("save_path", save_path);
        file_data.append('time_name', time_name);
        //給予 #image_path 值，等等存檔時會用
        $("#image_path").val(save_path + time_name + file_name);
        // 選了圖片不能選影片
        $("input[name='video_path']").prop("disabled", true);
        // 先顯示
        $("#add_article_form .image_box").show();
        //在圖片區塊，顯示loading
        $("div.image").html('<div class="spinner-border text-primary" role="status"><span class="visually-hidden">Loading...</span></div>');

        // 获取用户选择的文件
        var file = this.files[0];
        // 使用FileReader读取文件内容，并将其显示在图片预览中
        if (file) {
          var reader = new FileReader();
          reader.onload = function(e) {
            //將檔案插入
            $("div.image").html("<img src='" + e.target.result + "'>");
          };
          reader.readAsDataURL(file);
        }
      });

      /**
       * 影片上傳
       */
      //上傳圖片的input更動的時候
      $("input[name='video_path']").on("change", function() {
        // 有選檔案才繼續
        if ($("input[name='video_path']").val() === '') {
          //如果有影片路徑，就刪除該檔案
          if ($("#video_path").val() !== '') {
            // 清空图片预览
            $("div.video").html('');
            // 清空存檔路徑
            $("#video_path").val('');
            // 重置文件输入框
            $('#category_video').val('')
            // 隱藏
            $("#add_article_form .video_box").hide();
            // 啟用input
            $("input[name='image_path']").prop("disabled", false);
          }
          return
        }

        //產生 FormData 物件
        var file_name = $(this)[0].files[0]['name'];
        var save_path = "files/videos/";
        //FormData 新增剛剛選擇的檔案
        file_data.append("file", $(this)[0].files[0]);
        file_data.append("save_path", save_path);
        file_data.append('time_name', time_name);
        //給予 #video_path 值，等等存檔時會用
        $("#video_path").val(save_path + time_name + file_name);

        // 選了影片不能選圖片
        $("input[name='image_path']").prop("disabled", true);
        // 先顯示
        $("#add_article_form .video_box").show();
        //在圖片區塊，顯示loading
        $("div.video").html('<div class="spinner-border text-primary" role="status"><span class="visually-hidden">Loading...</span></div>');

        // 获取用户选择的文件
        var file = this.files[0];
        // 使用FileReader读取文件内容，并将其显示在图片预览中
        if (file) {
          var reader = new FileReader();
          reader.onload = function(e) {
            //將檔案插入
            $("div.video").html("<video src='" + e.target.result + "' controls>");
          };
          reader.readAsDataURL(file);
        }
      });

      /**
       * 刪除照片
       */
      $("a.del_image").on("click", function() {
        //如果有圖片路徑，就刪除該檔案
        if ($("#image_path").val() !== '') {
          // 清空图片预览
          $("div.image").html('');
          // 清空存檔路徑
          $("#image_path").val('');
          // 重置文件输入框
          $('#category_image').val('')
          // 隱藏
          $("#add_article_form .image_box").hide();
          // 啟用input
          $("input[name='video_path']").prop("disabled", false);
        } else {
          alert("無檔案可以刪除");
        }
      });

      /**
       * 刪除影片
       */
      $("a.del_video").on("click", function() {
        //如果有影片路徑，就刪除該檔案
        if ($("#video_path").val() !== '') {
          // 清空图片预览
          $("div.video").html('');
          // 清空存檔路徑
          $("#video_path").val('');
          // 重置文件输入框
          $('#category_video').val('')
          // 隱藏
          $("#add_article_form .video_box").hide();
          // 啟用input
          $("input[name='image_path']").prop("disabled", false);
        } else {
          alert("無檔案可以刪除");
        }
      });

      //表單送出
      $("#add_article_form").on("submit", function() {
        //加入loading icon
        $("div.loading").html('<div class="spinner-border text-primary" role="status"><span class="visually-hidden">Loading...</span></div>');

        if ($("#title").val() == '' || $("#intro").val() == '' || $('#category_video').val() === '' && $('#category_image').val() === ''){
          alert("作品資訊未填寫完整");

          //清掉 loading icon
          $("div.loading").html('');
        } else {
          // 實際存檔
          $.ajax({
            type: 'POST',
            url: '../php/upload_file.php',
            data: file_data,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'html'
          }).done(function(data) {
            //上傳成功
            if (data == "yes") {
              // 使用 ajax 送出 帳密給 verify_user.php
          //使用 ajax 送出 帳密給 verify_user.php
          $.ajax({
            type: "POST",
            url: "../php/add_work.php", //因為此檔案是放在 admin 資料夾內，若要前往 php，就要回上一層 ../ 找到 php 才能進入 add_article.php
            data: {
              title: $("#title").val(), //介紹
              intro: $("#intro").val(), //介紹
              image_path: $("#image_path").val(), //圖片路徑
              video_path: $("#video_path").val(), //影片路徑
              publish: $("input[name='publish']:checked").val() //發布狀況
            },
            dataType: 'html' //設定該網頁回應的會是 html 格式
          }).done(function(data) {

            //成功的時候
            if (data == "yes") {
              //註冊新增成功，轉跳到登入頁面。
              alert("新增成功，點擊確認回列表");
              window.location.href = "work_list.php";
            } else {
              alert("更新錯誤");
              console.log(data);
            }

          }).fail(function(jqXHR, textStatus, errorThrown) {
            //失敗的時候
            alert("有錯誤產生，請看 console log");
            console.log(jqXHR.responseText);
          });
        } else {
              // 警告回傳的訊息
              alert(data);
            }
          }).fail(function(data) {
            // 失敗的時候
            alert("有錯誤產生，請看 console log");
            console.log(data.responseText);
          });
        }
        return false;
      });
    });
  </script>
</body>

</html>